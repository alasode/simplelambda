using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SimpleLambda
{
    public class Function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>

        private static string GetConnectionString()
        {
            return System.Environment.GetEnvironmentVariable("championProductsDBConnectionString");
        }

        public bool FunctionHandler(ILambdaContext context)
        {
            context.Logger.Log("Begin function");

            string connStr = GetConnectionString();
                var InvoiceIds = new List<string>();

            //return input?.ToUpper();
            bool isSuccess;
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using var Cmd = new SqlCommand($"SELECT invoiceId from invoice", con);
                    con.Open();

                    SqlDataReader rdr = Cmd.ExecuteReader();

                    //loop through the results
                    while (rdr.Read())
                    {
                        InvoiceIds.Add(rdr[0].ToString());
                    }
                }

                var conn = new SqlConnection(connStr);

                foreach (var id in InvoiceIds)
                {
                    using var updateCmd = new SqlCommand($"UPDATE dbo.Invoice SET PaidDate = GETDATE() WHERE invoiceId = " + id, conn);
                    conn.Open();
                    updateCmd.ExecuteNonQuery();
                    conn.Close();

                }

                isSuccess = true;
            }
            catch (Exception)
            {

                isSuccess = false;
            }


            return isSuccess;
         }
     }
}